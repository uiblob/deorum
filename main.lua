fennel = require("lib.fennel.fennel")

realrequire = require

function require (libname)
   local path = "src/"..libname
   path = path:gsub ("%.", "/")..".fnl"
   if (love.filesystem.getInfo (path)) then
      return fennel.eval (love.filesystem.read (path), {filename=path})
   else
      return realrequire (libname)
   end
end

require "game"
