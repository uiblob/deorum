--- Check if a file or directory exists in this path
function exists(file)
   local ok, err, code = os.rename(file, file)
   if not ok then
      if code == 13 then
         -- Permission denied, but it exists
         return true
      end
   end
   return ok, err
end

--- Check if a directory exists in this path
function isdir(path)
   -- "/" works on both Unix and Windows
   return exists(path.."/")
end

function ps_execute(script)
   local pipe = io.popen("powershell -command -", "w")
   pipe:write(script)
   pipe:close()
end


if isdir("lib") then
   local path_separator = package.config:sub(1,1)
   if path_separator == "\\" then
      ps_execute("rm -Force -Recurse lib")
   else
      os.execute("rm -rf lib")
   end
end

os.execute("mkdir lib")

local git_dependencies = {
   fennel = "https://github.com/bakpakin/Fennel",
   Input = "https://github.com/SSYGEN/boipushy",
   lume = "https://github.com/rxi/lume/",
   i18n = "https://github.com/kikito/i18n.lua",
   hump = "https://github.com/vrld/hump",
}

for k,v in pairs(git_dependencies) do
   os.execute(string.format("git clone %s lib/%s",v,k))
end
