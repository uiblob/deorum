#!/bin/sh

mkdir -p bundles

LOVE_VERSION_DL="11.1"
LOVE_VERSION_ZP="11.1.0"

zip -9 -r deorum.love .
zip -d deorum.love "tools"
zip -d deorum.love "Makefile"
zip -d deorum.love ".git"
zip -d deorum.love ".gitattributes"
zip -d deorum.love ".gitignore"
zip -d deorum.love ".gitlab-ci.yml"
zip -d deorum.love "README.md"
cp deorum.love bundles/deorum.love

# PACK W64 RELEASE
W64Folder="love-$LOVE_VERSION_ZP-win64"
wget --no-check-certificate https://bitbucket.org/rude/love/downloads/love-$LOVE_VERSION_DL-win64.zip -O love_w_64.zip
unzip love_w_64.zip
cat $W64Folder/love.exe deorum.love > $W64Folder/deorum.exe
cd love-$LOVE_VERSION_ZP-win64
cat love.exe ../deorum.love > deorum.exe
zip -9 -r ../bundles/w64.zip *
cd ../bundles
zip -d w64.zip "changes.txt"
zip -d w64.zip "readme.txt"
zip -d w64.zip "love.exe"
zip -d w64.zip "lovec.exe"
cd ..

# PACK W64 RELEASE
W32Folder="love-$LOVE_VERSION_ZP-win32"
wget --no-check-certificate https://bitbucket.org/rude/love/downloads/love-$LOVE_VERSION_DL-win32.zip -O love_w_32.zip
unzip love_w_32.zip
cat $W32Folder/love.exe deorum.love > $W32Folder/deorum.exe
cd love-$LOVE_VERSION_ZP-win32
cat love.exe ../deorum.love > deorum.exe
zip -9 -r ../bundles/w32.zip *
cd ../bundles
zip -d w32.zip "changes.txt"
zip -d w32.zip "readme.txt"
zip -d w32.zip "love.exe"
zip -d w32.zip "lovec.exe"
cd ..
