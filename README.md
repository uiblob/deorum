![deorum logo](https://uiblob.gitlab.io/deorum/logo.svg)

# Deorum

A rougelite within a survival game within a procedural story within a rouglike. _Have fun dying_.

## Changelog

See CHANGELOG file.

## Downloads

Download, unpack zip and play. Installers are currently not available.

- [.love file for LÖVE2D 11.1](https://uiblob.gitlab.io/deorum/deorum.love)
- [Windows 64bit](https://uiblob.gitlab.io/deorum/deorum_windows_64.zip)
- [Windows 32bit](https://uiblob.gitlab.io/deorum/deorum_windows_32.zip)
- Mac OS X (comming soon)
- Linux (comming soon)

## Open Source

The code of the project is open source. The upstream is [gitlab.com/uiblob/deorum](https://gitlab.com/uiblob/deorum). Feel free to fork.
