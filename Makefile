run:
	love .

deps:
	lua5.1 tools/build/dependencies.lua

bundle: deps
	sh tools/build/bundle.sh

dockerimage:
	docker build -t registry.gitlab.com/uiblob/deorum tools/build-docker/
	docker push registry.gitlab.com/uiblob/deorum

website:
	mkdir -p public
	cp CHANGELOG public/changelog.txt
	cp tools/website/style.css public/style.css
	cp tools/website/logo.svg public/logo.svg
	cp bundles/deorum.love public/deorum.love
	cp bundles/w32.zip public/deorum_windows_32.zip
	cp bundles/w64.zip public/deorum_windows_64.zip
	cat tools/website/head.html > public/index.html
	markdown README.md > README.html
	cat CHANGELOG | sed '/^$$/q' > newchangelog.txt
	csplit README.html '/CHANGELOG/'
	cat xx00 >> public/index.html
	echo "<pre>" >> public/index.html
	cat newchangelog.txt >> public/index.html
	echo "</pre>" >> public/index.html
	echo '</p><p><a href="changelog.txt">Open full changelog.</a>'
	tail -n +2 xx01 >> public/index.html
	cat tools/website/foot.html >> public/index.html
