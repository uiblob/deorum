Fork it, do what you want to do with it (please be sane) and make a merge
request to the develop branch of https://gitlab.com/uiblob/deorum.

Add you changes ABOVE the last version number of CHANGELOG. For each feature
add a dash and a description for players, not developers.

Feel free to add shortcuts do love functions in your code but merge requests
using those shortcuts (or containing shortcut definitions) won't be accepted.