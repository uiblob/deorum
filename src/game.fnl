;; Create saves folder if not existent.
(let [pathinfo (love.filesystem.getInfo (love.filesystem.getSaveDirectory directory))]
  (when (not pathinfo)
    (love.filesystem.createDirectory (love.filesystem.getSaveDirectory))))

;; Note: ../conf.lua is parsed before this runs!
;; Note: love.load runs in this.
(require "globals")

;; Start with the first scene.
(scene.switch ddefaultscene)

(defn love.draw []
  (scene.draw))

(defn love.update [dt]
  (scene.update dt))
