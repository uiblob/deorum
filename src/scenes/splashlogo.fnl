(var logo (love.graphics.newImage "assets/graphics/logo.png"))
(local imgh (: logo :getHeight))
(local imgw (: logo :getWidth))
(local posy (fn [] (- (/ dwindow-height 2) (/ imgh 2))))
(local posx (fn [] (- (/ dwindow-width 2) (/ imgw 2))))
(local bindings {:space 'skip'
                 :fdown 'skip'})

(var time 0)
(var currentalpha 0)
(var input nil)

(local next-scene
       (lambda []
         (if (love.filesystem.getInfo "mode")
             (scene.switch "mainmenu")
             (scene.switch "onboarding-fullscreen"))))

(defn init []
  (set input (hid))
  (each [key action (pairs bindings)]
      (: input :bind key action))
   (love.graphics.setBackgroundColor ddefaultbg))

(fn draw []
  (love.graphics.setColor 1 1 1 currentalpha)
  (love.graphics.draw logo (posx) (posy)))

(fn update [dt]
  (when (: input :pressed 'skip') (next-scene))
  (set time (+ time dt))
  (if (< time 1)
      (set currentalpha time)
      (when (< 3 time)
        (set currentalpha (- currentalpha dt))
        (when (< 4 time)
          (next-scene)
          ))))

(fn clean []
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.setBackgroundColor 0 0 0))

{:clean clean
 :draw draw
 :init init
 :update update}
