(var answered false)
(var selected-option 1)
(var options {true false})

(local bindings {
                 :left "next"
                 :right "next"
                 :dpleft "next"
                 :dpright "next"
                 :start "accept"
                 :space "accept"
                 })

(var input nil)

(defn init []
  (set input (hid))
  (each [key action (pairs bindings)]
      (: input :bind key action))
  (love.graphics.setBackgroundColor ddefaultbg)
  (when (. (scene.get-arguments) fullscreen) (love.window.setMode 0 0 {:fullscreen true}))
  )

(defn draw []
  (love.graphics.setColor 1 1 1 1)
  (dlib.font 48)
  (love.graphics.printf "Choose the resolution." 16 16 (- dwindow-width 16) "center")
  (dlib.font 28)
  (love.graphics.printf "Use the arrow keys or dpad to select. Confirm with space or start." 16 (+ 16 48 16) (- dwindow-width 16) "center")
  (dlib.font 48)
  (when (= 2 selected-option) (love.graphics.setColor 1 1 1 .5) )
  (love.graphics.printf "Yes"
                        16
                        (+ 16 16 16 28 48)
                        (- (/ dwindow-width 2) 16)
                        "center")
  (if (= 1 selected-option)
      (love.graphics.setColor 1 1 1 .5)
      (love.graphics.setColor 1 1 1 1))
  (love.graphics.printf "No"
                        (+ 16 16(/ dwindow-width 2))
                        (+ 16 16 16 28 48)
                        (- (/ dwindow-width 2) 32)
                        "center")
  )

(defn confirm []
  (scene.switch "mainmenu" ))

(defn next-option []
  (if (< selected-option 2)
      (set selected-option 2)
      (set selected-option 1)))

(defn update [dt]
  (when (: input :pressed "next") (next-option))
  (when (: input :pressed "accept") (confirm))
  )


(fn clean []
  (love.graphics.setBackgroundColor 0 0 0))

{:draw draw
 :init init
 :clean clean
 :update update}
