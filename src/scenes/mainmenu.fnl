(var input nil)

(defn draw []
    (love.graphics.print "Hello my lisp love" 100 100))


(defn update [dt]
  (when (: input :pressed 'exit') (dlib.end-game))
  (when (: input :pressed 'debug') (debug.debug)))

(local bindings {
                 :q 'exit'
                 :d 'debug'
                 })



(defn init []
  (set input (hid))
   (each [key action (pairs bindings)]
      (: input :bind key action)))


{:bindings bindings
 :draw draw
 :init init
 :update update}
