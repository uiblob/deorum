;; Global constants which don't need to compute a value.
(global ddebuglog true)
(global dlogfile "log.txt")
(global ddefaultscene "splashlogo")

;; Load lua libraries 
(global lume   (require "lib.lume.lume"))
(global hid (require "lib.Input.Input"))


;; Load fennel libraries
(global dlog (require "log"))
(global dlib   (require "deorumlib"))
(global scene  (require "scene"))

(let [(width height) (love.window.getMode)]
  (global dwindow-height height)
  (global dwindow-width width))

(global ddefaultbg (dlib.rgb-255-to-1 51 57 65))

(love.graphics.setDefaultFilter "nearest" "nearest" 0)

(defn love.load []
  (dlog.info "L�VE is loaded."))
