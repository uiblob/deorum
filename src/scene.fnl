;; PRIVATE CONSTANTS

;;; Set the time for the transition between scenes.
(local transitiontime 1)

;;; Create a canvas which can hold the screenshot of the last scene 
;;; for transitions.
(local lastscene (love.graphics.newCanvas dwindow-width dwindow-height))

;; PRIVATE VARIABLES

;;; This variable will hold the module for the current scene.
(var currentscene false)

;;; Holds the arguments to a scene.
(var scene-arguments {})

;;; This variable holds the second since the last scene switch, but only up 
;;; to transitiontime.
(var time-since-switch transitiontime)

;; PRIVATE FUNCTIONS

;;; Callback used for rendering the screenshot onto the lastscene canvas.
(defn save-screenshot [screenshot]
  (let [screenshotimage (love.graphics.newImage screenshot)]
    (love.graphics.setCanvas lastscene)
    (love.graphics.clear)
    (love.graphics.draw screenshotimage)
    (love.graphics.setCanvas)))

;;; Saves the arguments to a scene.
(defn set-argtable [?argtable]
  (set scene-arguments 
       (if ?argtable
           ?argtable
           {})))

;; PUBLIC FUNCTIONS

;;; Switch from one scene to another.
(defn switch [targetstate ?argtable]
  (love.graphics.captureScreenshot save-screenshot)
  (when currentscene 
    (currentscene.clean))
  (global sceneargs (if ?argtable ?argtable {}))
  (set currentscene (require (.. "scenes." targetstate)))
  (currentscene.init)
  (set time-since-switch 0))

;;; Read the arguments to the current scene.
(defn get-arguments [] scene-arguments)

;;; Draw the current scene and transition.
(defn draw []
  (if currentscene
      (currentscene.draw)
      (love.graphics.print "No state!" 100 100 ))
  (when (< time-since-switch transitiontime)
    (love.graphics.setColor 1 1 1 (- 1 time-since-switch))
    (when lastscene (love.graphics.draw lastscene))
    (love.graphics.setColor 1 1 1 1)))

;;; Update the current scene and transition.
(defn update [dt]
  (when (< time-since-switch transitiontime)
    (set time-since-switch (+ time-since-switch dt)))
  (when currentscene (currentscene.update dt)))

;; RETURN MODULE
{:switch switch
 :get-arguments get-arguments
 :update update
 :draw draw}
