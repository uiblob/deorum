(when ddebuglog
  (love.filesystem.write dlogfile "DEORUM LOG"))

(var dlog {})

(defn logwrite [sign message]
  (when ddebuglog
    (let [time (os.date "%y-%m-%d-%X")
          msgstring (string.format "(%s) %s %s\r\n" sign time message)]
      (love.filesystem.append dlogfile msgstring))))

(defn dlog.error [message]
  (logwrite "E" message))

(defn dlog.info [message]
  (logwrite "I" message))

(dlog.info "Started logging.")

dlog
