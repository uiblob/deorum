; Here the logic of quiting the game is implemented
(defn end-game []
  (love.event.quit))

(defn rgb-255-to-1 [r g b]
  [(/ r 255)
   (/ g 255)
   (/ b 255)])

(defn font [size]
  (love.graphics.setFont (love.graphics.newFont "assets/fonts/TightPixel/tight_pixel.ttf" size)))

{
 :end-game end-game
 :rgb-255-to-1 rgb-255-to-1
 :font font
}
  
