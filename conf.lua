function love.conf(t)
   t.identity = deorum          -- The name of the save directory (string)
   t.modules.physics = false	-- Disable physics module
   t.window.fullscreen = false   -- Enable fullscreen (boolean)
   t.console = true
   t.window.width = 640               -- The window width (number)
   t.window.height = 480               -- The window height (number)
--   t.window.borderless = true
   t.window.minwidth = 640
   t.window.minheight = 480
end
